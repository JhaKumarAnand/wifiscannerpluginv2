import 'dart:async';
import 'dart:convert';
import 'package:flutter/services.dart';

class WifiScanner {
  static const MethodChannel _channel = MethodChannel('wifi_scanner');

  static Future<String> validateLicense(String license) async {
    final String jsonString =
        await _channel.invokeMethod('validateLicense', <String, dynamic>{'license': license}) ?? '';
    final dynamic json =
        jsonString.contains('state') ? jsonDecode(jsonString) : <String, dynamic>{'state': 'Suspended'};
    return json['state'] as String;
  }

  static Future<bool> _isLicenseValid(String license) async => await validateLicense(license) == 'Ok';

  static Future<dynamic> startScanning(String license) async =>
      await _isLicenseValid(license) ? await _channel.invokeMethod('startScan') : '';

  static Future<void> stopScanning(String license) async =>
      await _channel.invokeMethod('stopScan', <String, dynamic>{'license': license});
}
