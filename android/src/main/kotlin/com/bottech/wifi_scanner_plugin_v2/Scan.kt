import android.content.Context
import android.util.Log
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import org.json.JSONObject
import com.overlook.android.fingkit.FingScanner
import com.overlook.android.fingkit.FingScanOptions
import com.overlook.android.fingkit.FingScanResultLevel

class Scan(private val context: Context) {

    private val scanner: FingScanner = FingScanner.getInstance()

    fun invoke(result: MethodChannel.Result, call: MethodCall) {
        if (scanner.isConnected) {
            invokeMethod(result, call)
            Log.i(LOG, "Agaile1111...")
        } else {
            Log.i(LOG, "Agaile2111...")
            Log.i(LOG, "Not connected. Connecting now...")
            scanner.connect(context) { _, e: Exception? ->
                if (e == null) invokeMethod(result, call)
                else onError(result, e)
            }
        }
    }

    private fun invokeMethod(result: MethodChannel.Result, call: MethodCall) = when (call.method) {
        START_METHOD -> start(result)
        STOP_METHOD -> stop()
        VALIDATE_METHOD ->  validateLicense(result, call.argument(LICENSE_KEY))
        else -> result.notImplemented()
    }

    fun disconnect() {
        if (scanner.isConnected) {
            Log.i(LOG, "FingScanner connected. Disconnecting now...")
            scanner.disconnect { _, e: Exception? ->
                if (e == null) Log.i(LOG, "Disconnected to FingScanner")
                else Log.wtf(LOG, "Failed to disconnect to FingScanner", e)
            }
        } else {
            Log.i(LOG, "FingScanner already disconnected")
        }
    }

    private fun validateLicense(result: MethodChannel.Result, license: String?) =
            if (license.isNullOrEmpty()) onError(result, Exception("License not provided"))
            else scanner.validateLicenseKey(license, null, validateCallback(result))

    val options:FingScanOptions = FingScanOptions().apply {
        resultLevelScanInProgress = FingScanResultLevel.FingScanResultNone
        resultLevelScanCompleted = FingScanResultLevel.FingScanResultFull
    }

    private fun start(result: MethodChannel.Result) = scanner.networkScan(options, scanCallback(result))

    private fun stop() = scanner.networkScanStop()

    private fun validateCallback(result: MethodChannel.Result): FingScanner.FingResultCallback? =
            FingScanner.FingResultCallback { s: String?, e: Exception? ->
                if (e == null) onSuccess(result, s)
                else onError(result, e)
            }

    private fun scanCallback(result: MethodChannel.Result): FingScanner.FingResultCallback? =
            FingScanner.FingResultCallback { s: String?, e: Exception? ->
                if (e == null) {
                    val obj = JSONObject(s)
                    if (obj.getBoolean("completed") && obj.getInt("progress") > 0) {
                        onSuccess(result, s)
                    }
                } else onError(result, e)
            }

    private fun onSuccess(result: MethodChannel.Result, s: String?) {
        result.success(s)
        if (s != null) {
            Log.i(LOG, s)
        }
    }

    private fun onError(result: MethodChannel.Result, e: Exception) {
        result.error("500", e.message, null);
        Log.wtf(LOG, "Error", e)
    }

    companion object {
        const val CHANNEL = "flutter.fing/scan"
        const val LOG = "network scan"

        const val LICENSE_KEY = "license"

        const val START_METHOD = "startScan"
        const val STOP_METHOD = "stopScan"
        const val VALIDATE_METHOD = "validateLicense"
    }
}