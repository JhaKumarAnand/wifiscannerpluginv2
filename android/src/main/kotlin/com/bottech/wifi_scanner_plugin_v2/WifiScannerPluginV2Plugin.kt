package com.bottech.wifi_scanner_plugin_v2

import Scan
import android.util.Log
import androidx.annotation.NonNull

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result

/** WifiScannerPluginV2Plugin */
class WifiScannerPluginV2Plugin: FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel
  private lateinit var scanner: Scan

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    scanner = Scan(flutterPluginBinding.applicationContext)
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "wifi_scanner")
    channel.setMethodCallHandler(this)
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    Log.e("TAG", "onMethodCall called");
    if (call.method == "startScan") {
      Log.e("TAG", "startScan called");
      scanner.invoke(result, call)
    } else if (call.method == "validateLicense") {
      Log.e("TAG", "startScan called");
      scanner.invoke(result, call)
    } else if (call.method == "stopScan") {
      Log.e("TAG", "startScan called");
      scanner.invoke(result, call)
    } else {
      result.notImplemented()
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    scanner.disconnect()
    channel.setMethodCallHandler(null)
  }
}
