import Flutter
import UIKit
import FingKit

public class SwiftWifiScannerPluginV2Plugin: NSObject, FlutterPlugin {

    let CHANNEL = "flutter.fing/scan"
    let LICENSE_KEY = "license"
    let START_METHOD = "startScan"
    let STOP_METHOD = "stopScan"
    let VALIDATE_METHOD = "validateLicense"
    let scanner = FingScanner.sharedInstance()
    let options = FingScanOptions.systemDefault()

  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "wifi_scanner", binaryMessenger: registrar.messenger())
    let instance = SwiftWifiScannerPluginV2Plugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
     options?.resultLevelScanInProgress = FingScanResultLevel.none
     options?.resultLevelScanCompleted = FingScanResultLevel.full
    if call.method == START_METHOD {
                    scanner.networkScan(options) { (output: String?, err: Error?) in
                        if err==nil{
                            if let dict = try! JSONSerialization.jsonObject(with: output!.data(using: .utf8)!, options: .allowFragments) as? [String: Any]{
                                // Some devices result is not coming back, Suspect progress may not reach 100.
                                if dict["completed"] as? String == "true" && (dict["progress"] as? Int ?? 0) > 0{
                                    result(output);
                                }
                            }
                        }else{
                            self.onError(result: result, err: err);
                        }
                    }
                }else if call.method == STOP_METHOD {
                    scanner.networkScanStop();
                }else if call.method == VALIDATE_METHOD {
                    guard let args = call.arguments else {
                        return
                    }
                    if let myArgs = args as? [String: Any],
                       let license = myArgs[LICENSE_KEY] as? String{
                        scanner.validateLicenseKey(license, withToken: nil, completion: {  (output:String? , err : Error?) in
                            if err==nil {
                                result(output);
                            }else{
                                self.onError(result: result, err: err);
                            }
                        })
                    } else {
                        result(FlutterError(code: "500", message: "iOS could not extract " +
                                                "flutter arguments in method: (sendParams)", details: nil))
                    }
                }else{
                    result(FlutterMethodNotImplemented)
                }

//    result("iOS" + UIDevice.current.systemVersion)
  }

    func onError(result:  @escaping FlutterResult, err: Error?) {
                result(FlutterError(code: "500", message: err?.localizedDescription, details: err.debugDescription))
            }
}
