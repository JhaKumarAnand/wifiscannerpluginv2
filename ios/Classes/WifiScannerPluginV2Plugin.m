#import "WifiScannerPluginV2Plugin.h"
#if __has_include(<wifi_scanner_plugin_v2/wifi_scanner_plugin_v2-Swift.h>)
#import <wifi_scanner_plugin_v2/wifi_scanner_plugin_v2-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "wifi_scanner_plugin_v2-Swift.h"
#endif

@implementation WifiScannerPluginV2Plugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftWifiScannerPluginV2Plugin registerWithRegistrar:registrar];
}
@end
