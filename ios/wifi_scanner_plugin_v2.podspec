#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint wifi_scanner_plugin_v2.podspec` to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'wifi_scanner_plugin_v2'
  s.version          = '0.0.1'
  s.summary          = 'A new Flutter plugin.'
  s.description      = <<-DESC
A new Flutter plugin.
                       DESC
  s.homepage         = 'http://example.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Your Company' => 'email@example.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.dependency 'Flutter'
  s.platform = :ios, '8.0'
  s.preserve_paths = 'FingKit.framework'
  s.xcconfig = { 'OTHER_LDFLAGS' => '-framework FingKit' }
  s.vendored_frameworks = 'FingKit.framework', 'libresolv.9.tdb', 'libsqllite3.tbd', 'SystemConfiguration.framework', 'Security.framework', 'Foundation.framework', 'CFNetwork.framework', 'CoreTelephony.framework'

  # Flutter.framework does not contain a i386 slice.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.swift_version = '5.0'
end
